package com.pens.favian.backgroundparallax;

import android.support.v7.app.AppCompatActivity;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;

public class ParallaxActivity extends AppCompatActivity {

   private ParallaxView1 parallaxView1;

   @Override
    protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       this.requestWindowFeature(Window.FEATURE_NO_TITLE);
       this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
       Display display = getWindowManager().getDefaultDisplay();
       Point resolution = new Point();
       display.getSize(resolution);
       parallaxView1 = new ParallaxView1(this, resolution.x, resolution.y);
       setContentView(parallaxView1);
   }

   @Override
    protected void onPause(){
       super.onPause();
       parallaxView1.pause();
   }

   @Override
    protected void onResume(){
       super.onResume();
       parallaxView1.resume();
   }
}
